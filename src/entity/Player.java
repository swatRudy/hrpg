package entity;

import java.util.Random;

public class Player {
	Random r = new Random();

	
	public Player p;
	private int PlayerHealth =35;
	private int PlayerDefense = r.nextInt(9);
	private int PlayerAttack = r.nextInt(20);
	private String PlayerName;
	private String weapon = "Rusty Dagger";
	
	
	public Player Player() {
		return p;
	}
	
	public String getName(){
		return PlayerName;
	}
	
	public void setName(String p){
		PlayerName = p;
	}
	
	public int getHealth(){
		return PlayerHealth;
	}
	
	public int getDefense(){
		return PlayerDefense;
	}
	public String getWeapon() {
		return weapon;
	}
	public int getAttack() {
		return PlayerAttack;
	}

}
