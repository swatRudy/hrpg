package entity.enemy.monsters.physical;

public class Goblin {
	
	private int hp = 25;
	private String name = "Goblin";
	private boolean isBoss = false;
	private int baseATK = 10;
	
	public int getHP(){
		return hp;
	}
	public String getName(){
		return name;
	}

	public boolean isBoss(){
		return isBoss;
	}
	public int getBaseATK(){
		return baseATK;
	}
	
	

}
