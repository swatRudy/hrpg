package entity.enemy.monsters;


import entity.enemy.monsters.physical.Ogre;
import entity.enemy.monsters.magic.Ghost;
import entity.enemy.monsters.magic.Witch;
import entity.enemy.monsters.other.Bat;
import entity.enemy.monsters.physical.Goblin;
import entity.enemy.monsters.boss.Latematt;

public class Monsters {
	
	Goblin g = new Goblin();
	Latematt late = new Latematt();
	Ghost gho = new Ghost();
	Ogre o = new Ogre();
	Witch w = new Witch();
	Bat b = new Bat();
	
	
	
	
	
	public Goblin getGoblin(){
		return g;
	}
	public Latematt getLate(){
		return late;
	}
	public Ghost getGhost(){
		return gho;
	}
	public Ogre getOgre(){
		return o;
	}
	public Witch getWitch(){
		return w;
	}
	public Bat getBat(){
		return b;
	}
	
	
		

}
