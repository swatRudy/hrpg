package entity.enemy;

public enum MonsterType {
	SPECIAL,
	MAGIC,
	PHYSICAL,
	BOSS,
	OTHER
}
