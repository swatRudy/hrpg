package base.battle.main;
import entity.*;
import entity.enemy.monsters.Monsters;
import entity.*;
import base.battle.Modifier;

import java.util.Random;
import java.util.Scanner;

public class battle {
    Scanner s = new Scanner(System.in);
    Random r = new Random();
    Monsters mons = new Monsters();
    Player p = new Player();
    Modifier mod = new Modifier();
    public String slashes[] = {"Bashes", "Slashes", "Cuts", "Stabs", "Scratches", "Pokes"};
    public String wild[] = {"Wild", "Corrupt", ""};
    //
    static int hp = 0;
    static int baseATK = 0;

    public static void printL(String s) {
        System.out.println(s);

    }

    public boolean battle(String monster) {
        boolean wonorNah = true;

        switch (monster) {
            case "Goblin":
                hp = mons.getGoblin().getHP();
                baseATK = mons.getGoblin().getBaseATK();
                break;
            case "Ogre":
                hp = mons.getOgre().getHP();
                baseATK = mons.getOgre().getBaseATK();
                break;
            case "Ghost":
                hp = mons.getGhost().getHP();
                baseATK = mons.getGhost().getBaseATK();
                break;
            case "Bat":
                hp = mons.getBat().getHP();
                baseATK = mons.getBat().getBaseATK();
                break;
            case "Witch":
                hp = mons.getWitch().getHP();
                baseATK = mons.getWitch().getBaseATK();
                break;
            case "Latematt":
                hp = mons.getLate().getHP();
                baseATK = mons.getLate().getBaseATK();
                break;
            default:
                hp = 10000000;
                baseATK = 10000000;
                break;

        }


        printL("You have encountered a wild " + monster + "!");
        printL("STATS: Base HP = " + hp + " Base ATK = " + baseATK + "!");


        while (p.getHealth() >= 0 && hp >= 0) {
            if (p.getHealth() < 0) {
                printL("You Died!");
                wonorNah = false;
            } else if (hp < 0) {
                printL("The enemies HP has reached Zero, Congratulations you won!");
                wonorNah = true;
            }
                printL("Do you want to attack or defend?");
                String choice = s.nextLine();
                if (mod.isEnraged() && mod.getFocus() > 50) {
                    printL("You unleash an attack that rips the " + monster + "apart, Instantly Killing it...");
                    System.exit(22);
                }
                if (choice.contains("att")) {
                    hp = attack(monster, hp);
                } else if (choice.contains("def")) {
                    defend();

                }
                enemyTurn(monster, hp, baseATK, p.getHealth());
            }


        return wonorNah;
        }

	
	public int attack(String m, int hp){
		printL( p.getName() + " " + slashes[r.nextInt(slashes.length)] + " the " + m + " For " + p.getAttack() +  " health using your " + p.getWeapon() + " !");
		hp = hp-p.getAttack();
        return hp;
	}
	public void defend(){
		printL(p.getName() + " defends for " +  p.getDefense() + " hp!");
	}


	public void enemyTurn(String s1, int i, int i2, int i3){
        printL("The " + s1 + " has " + i + " HP left!");
        printL("The "+ s1 + " " + slashes[r.nextInt(slashes.length)] + " you for " + i2 + " HP!");

    }
}
