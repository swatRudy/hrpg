package base.battle;

import java.util.Random;

public class Modifier {
	
	
	Random r = new Random();
	int focus = r.nextInt(100);
	int energy = r.nextInt(1000);
	boolean isEnraged;
	
	public int getEnergy() {
		return energy;
	}
	public int getFocus() {
		return focus;
	}

	public boolean isEnraged() {
		return isEnraged;
	}



}
