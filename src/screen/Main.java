package screen;

import java.util.Scanner;
import org.newdawn.slick.*;
import base.battle.main.battle;
import org.newdawn.slick.state.*;
import entity.Player;

import utiil.UtilRandom;
import utiil.UtilThread;

public class Main extends StateBasedGame {

	static Scanner s = new Scanner(System.in);
	static UtilThread uT = new UtilThread();
	static Player p = new Player();
	static UtilRandom uR = new UtilRandom();
    //game
    static final String name = "hRPG";
    //slick
    public static final int menu = 0;
    public static final int play = 1;

    public Main(String s1){
        super(s1);
        this.addState(new Menu(menu));
        this.addState(new Play(play));
        this.enterState(menu);
    }

    public void initStatesList(GameContainer gc) throws SlickException{
        this.getState(menu).init(gc, this);
        this.getState(play).init(gc, this);
    }




	public static void main(String[] args){
        AppGameContainer a;
        try{
            a = new AppGameContainer(new Main(name));
            a.setDisplayMode(400, 400, false);
            a.start();


        }catch(SlickException e){
            e.printStackTrace();
        }


	}

}
